set(EXAMPLE_SRCS
    preview-action.cpp
)

add_executable(preview-action ${EXAMPLE_SRCS})
target_link_libraries(preview-action lomiri-action-qt)
target_link_libraries(preview-action Qt::Core Qt::Widgets)
