/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_QML_ACTION_CONTEXT
#define LOMIRI_ACTION_QML_ACTION_CONTEXT

namespace lomiri {
namespace action {
namespace qml {
    class ActionContext;
}
}
}

#include <QQmlListProperty>

#include <lomiri/action/ActionContext>
#include <lomiri/action/Action>

# if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#define QQMLISTPROPERTY_INT qsizetype
# else
#define QQMLISTPROPERTY_INT int
# endif

class Q_DECL_EXPORT lomiri::action::qml::ActionContext : public lomiri::action::ActionContext
{
    Q_OBJECT
    Q_DISABLE_COPY(ActionContext)

    Q_PROPERTY(QQmlListProperty<lomiri::action::Action> actions
               READ action_list)

    Q_CLASSINFO("DefaultProperty", "actions")

public:
        explicit ActionContext(QObject *parent = 0);
        virtual ~ActionContext();

        QQmlListProperty<lomiri::action::Action> action_list();

private:

    static void append(QQmlListProperty<lomiri::action::Action> *list,lomiri::action::Action *action);
    static void clear(QQmlListProperty<lomiri::action::Action> *list);
    static QQMLISTPROPERTY_INT count(QQmlListProperty<lomiri::action::Action> *list);

};
#endif
