set(CORE_SRCS
    plugin.cpp
    qml-action.cpp
    qml-preview-action.cpp
    qml-manager.cpp
    qml-context.cpp
)

# Build everything twice. Since we have only a few
# source files, this is not an issue. If it does
# become one, change tools so they use the plugin
# instead of calling the classes directly.

add_library(lomiri-action-qml SHARED
${CORE_SRCS}
)
target_link_libraries(lomiri-action-qml lomiri-action-qt)

target_link_libraries(lomiri-action-qml Qt::Qml)

get_target_property(QMAKE_EXECUTABLE Qt::qmake LOCATION)
exec_program(${QMAKE_EXECUTABLE} ARGS "-query QT_INSTALL_QML" OUTPUT_VARIABLE QT_IMPORTS_DIR)
file(TO_CMAKE_PATH "${QT_IMPORTS_DIR}" QT_IMPORTS_DIR)

install(
  TARGETS lomiri-action-qml
  ARCHIVE DESTINATION ${QT_IMPORTS_DIR}/Lomiri/Action
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION ${QT_IMPORTS_DIR}/Lomiri/Action
)

install(
  FILES qmldir
  DESTINATION ${QT_IMPORTS_DIR}/Lomiri/Action
)

# copy qmldir file into build directory for shadow builds
file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/qmldir"
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
    )
